# "Front-end workflow with Gulp"
This repository contains the material used for the talk "Front-end workflow with Gulp" for the [Yelsterdigital GmbH](http://www.yelsterdigital.com/) developers workshop on the 24th and 25th September 2015 in Salzburg (Austria)

# "The State of JavaScript in 2015"
This repository contains the material used for the talk "The State of JavaScript in 2015" for the [Yelsterdigital GmbH](http://www.yelsterdigital.com/) developers workshop on the 24th and 25th September 2015 in Salzburg (Austria)
